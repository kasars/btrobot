/* ============================================================
 *
 * Copyright (C) 2018 by Kåre Särs <kare.sars@iki.fi>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License.
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * ============================================================ */

#include "VarSpeedServo.h"
#include "QBlueStickReader.h"
#include <EEPROM.h>  // This is kinda bugy when I need to include this for it to be added to the include... 
#include "EePromHandler.h"

VarSpeedServo servo1;
VarSpeedServo servo2;
VarSpeedServo servo3;
VarSpeedServo servo4;

int sVal1 = 120;  // Klo
int sVal2 = 90;  // Liten Arm
int sVal3 = 90;   // Store Arm
int sVal4 = 90;   // Tornet
int sTargetVal1 = 120;  // Klo
int sTargetVal2 = 90;  // Liten Arm
int sTargetVal3 = 90;   // Store Arm
int sTargetVal4 = 90;   // Tornet

int buttonPin = 4;

int playIndex = 0;

unsigned long lastMoveTime1;
unsigned long lastMoveTime2;
unsigned long lastMoveTime3;
unsigned long lastMoveTime4;
bool playingSequence = true;
bool playLoop = true;
unsigned int movingDelay = 40; // delay between changing one degree

QBlueStickReader stick;
EePromHandler eepromHandler;

String command;


boolean movingServo(VarSpeedServo &servo, int &sVal, int sTargetVal, unsigned long &lastMoveTime, unsigned int moveDelay)
{
    int aDiff = sTargetVal - sVal;
    unsigned long now = millis();
    unsigned long tDiff = now - lastMoveTime; // this might rol around but should not be a problem
    if (aDiff == 0 && tDiff >= moveDelay) {
        return false;
    }
    if (tDiff >= moveDelay) {
        sVal += (aDiff > 0 ? 1: -1);
        lastMoveTime = now;
        servo.write(sVal);
    }
    return true;
}

void printValues() 
{
    Serial.print("->(");
    Serial.print(sTargetVal1);
    Serial.print(", ");
    Serial.print(sTargetVal2);
    Serial.print(", ");
    Serial.print(sTargetVal3);
    Serial.print(", ");
    Serial.print(sTargetVal4);
    Serial.println(");");
}


void printSequence()
{
    int i = 0;
    int x1,x2,x3,x4;
    while (eepromHandler.getNextSequence(i, x1, x2, x3, x4) != 0) {
        Serial.print("s");
        Serial.print(x1);
        Serial.print(",");
        Serial.print(x2);
        Serial.print(",");
        Serial.print(x3);
        Serial.print(",");
        Serial.println(x4);
    };
}

void parseAddSequence(const String &str)
{
    String s = str.substring(1);
    int vals[4];
    for (int i=0; i<4; ++i) {
        vals[i] = s.toInt();
        int next = s.indexOf(',');
        if (next != -1 && next < s.length()-2) {
            s = s.substring(next+1);
        }
        else if (i!=3) {
            Serial.println("Failed to parse");
            return;
        }
    }
    eepromHandler.addPoint(vals[0], vals[1], vals[2], vals[3]);
}

void customDataReceived(char inData)
{
    if (inData == '\n') {
        if (command == "p") {
            printSequence();
        }
        else if (command == "n") {
            eepromHandler.newSequence();
        }
        else if (command.startsWith("s")) {
            parseAddSequence(command);
        }
        else {
            Serial.println("unknown command");
        }
        command = "";
    }
    else {
        command += inData;
    }
}


void setup()
{
    servo1.attach(11);
    servo2.attach(10);
    servo3.attach(9);
    servo4.attach(6);

    pinMode(buttonPin, INPUT_PULLUP);


    Serial.begin(9600);

    stick.setButtonText(0, "Loop");
    stick.setButtonText(1, "New Seq");
    stick.setButtonText(2, "Add");
    stick.setButtonText(3, "Play");
    stick.setButtonText(4, "Pause");
    stick.setButtonText(5, "Stop");
    stick.setButtonText(6, "Faster");
    stick.setButtonText(7, "Slower");

    stick.setCustomDataCalback(&customDataReceived);
}


void serialEvent() {
    while (Serial.available()) {
        stick.parseChar((char)Serial.read());
    }
}

void loop()
{
    boolean moving1 = true;
    boolean moving2 = true;
    boolean moving3 = true;
    boolean moving4 = true;

    int input = digitalRead(buttonPin);
    if (input == 1) {
        moving1 = movingServo(servo1, sVal1, sTargetVal1, lastMoveTime1, movingDelay);
        moving2 = movingServo(servo2, sVal2, sTargetVal2, lastMoveTime2, movingDelay);
        moving3 = movingServo(servo3, sVal3, sTargetVal3, lastMoveTime3, movingDelay);
        moving4 = movingServo(servo4, sVal4, sTargetVal4, lastMoveTime4, movingDelay);
    }

    if (playingSequence) {
        if (!moving1 && !moving2 && !moving3 && !moving4) {
            eepromHandler.getNextSequence(playIndex, sTargetVal1, sTargetVal2, sTargetVal3, sTargetVal4);
            if (playIndex == 0 && !playLoop) {
                playingSequence = false;
            }
            Serial.print(playIndex/4, DEC);
            Serial.print(" ");
            printValues();
        }
    }
    else {
        boolean change = false;
        if (!moving1) {
            if (stick.axis1Value() < -80 && sTargetVal1 < 180) { sTargetVal1++; change=true; }
            if (stick.axis1Value() > +80 && sTargetVal1 > 0)   { sTargetVal1--; change=true; }
        }

        if (!moving2) {
            if (stick.axis2Value() < -80 && sTargetVal2 < 180) { sTargetVal2++; change=true; }
            if (stick.axis2Value() > +80 && sTargetVal2 > 0)   { sTargetVal2--; change=true; }
        }

        if (!moving3) {
            if (stick.axis4Value() < -80 && sTargetVal3 < 180) { sTargetVal3++; change=true; }
            if (stick.axis4Value() > +80 && sTargetVal3 > 0)   { sTargetVal3--; change=true; }
        }

        if (!moving4) {
            if (stick.axis3Value() < -80 && sTargetVal4 < 180) { sTargetVal4++; change=true; }
            if (stick.axis3Value() > +80 && sTargetVal4 > 0)   { sTargetVal4--; change=true; }
        }
        if (change == true) {
            printValues();
        }
    }

    if (stick.buttonToggledDown(0)) {
        playLoop = true;
        playIndex = 0;
        playingSequence = true;
        Serial.println("Play Loop");
    }
    if (stick.buttonToggledDown(1)) {
        eepromHandler.newSequence();
    }
    if (stick.buttonToggledDown(2)) {
        eepromHandler.addPoint(sVal1, sVal2, sVal3, sVal4);
    }
    if (stick.buttonToggledDown(3)) {
        playingSequence = true;
        playIndex = 0;
        playLoop = false;
        Serial.println("Play Once");
    }
    if (stick.buttonToggledDown(4)) {
        playingSequence = !playingSequence;
        Serial.println(playingSequence ? "Play" : "Pause");
    }
    if (stick.buttonToggledDown(5)) {
        playingSequence = false;
        playIndex = 0;
        playLoop = false;
        Serial.println("Stop");
    }
    if (stick.buttonToggledDown(6)) {
        movingDelay--;
        if (movingDelay < 1) {movingDelay = 1;}
        Serial.print("Move delay: ");
        Serial.println(movingDelay, DEC);
    }
    if (stick.buttonToggledDown(7)) {
        movingDelay++;
        Serial.print("Move delay: ");
        Serial.println(movingDelay, DEC);
    }
}







