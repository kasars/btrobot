/* ============================================================
 *
 * Copyright (C) 2018 by Kåre Särs <kare.sars@iki.fi>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA or see <http://www.gnu.org/licenses/>
 *
 * ============================================================ */

#include "EePromHandler.h"
#include <EEPROM.h>


EePromHandler::EePromHandler(): m_writeIndex(0) {}

void EePromHandler::newSequence() {
    m_writeIndex = 0;
    Serial.println("New sequence");
}

void EePromHandler::addPoint(int val1, int val2, int val3, int val4)
{
    Serial.print("Add point:");
    Serial.print(val1);
    Serial.print(", ");
    Serial.print(val2);
    Serial.print(", ");
    Serial.print(val3);
    Serial.print(", ");
    Serial.println(val4);

    EEPROM.write(m_writeIndex+0, val1);
    EEPROM.write(m_writeIndex+1, val2);
    EEPROM.write(m_writeIndex+2, val3);
    EEPROM.write(m_writeIndex+3, val4);
    EEPROM.write(m_writeIndex+4, 255);
    EEPROM.write(m_writeIndex+5, 255);
    EEPROM.write(m_writeIndex+6, 255);
    EEPROM.write(m_writeIndex+7, 255);
    m_writeIndex += 4;
}


int EePromHandler::getNextSequence(int &index, int &s1, int &s2, int &s3, int &s4)
{
    int tmp1 = EEPROM.read(index+0);
    int tmp2 = EEPROM.read(index+1);
    int tmp3 = EEPROM.read(index+2);
    int tmp4 = EEPROM.read(index+3);

    if (tmp1 == 255 || tmp2 == 255 || tmp3 == 255 || tmp4 == 255) {
        index = 0;
        return index;
    }

    s1 = tmp1;
    s2 = tmp2;
    s3 = tmp3;
    s4 = tmp4;
    index+=4;

    return index;
}

