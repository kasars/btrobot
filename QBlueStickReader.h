/* ============================================================
 *
 * Copyright (C) 2018 by Kåre Särs <kare.sars@iki.fi>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA or see <http://www.gnu.org/licenses/>
 *
 * ============================================================ */
#ifndef QBlueStickReader_h
#define QBlueStickReader_h

#include <Arduino.h>

class QBlueStickReader {

public:
    QBlueStickReader();

    void setButtonText(int button, const String &text);

    bool buttonIsDown(int button);
    bool buttonToggledDown(int button);
    bool buttonToggledUp(int button);

    int axis1Value() { return m_axis1; }
    int axis2Value() { return m_axis2; }
    int axis3Value() { return m_axis3; }
    int axis4Value() { return m_axis4; }

    bool newInputData();

    void printConfig();
    void printStatus();
    void parseChar(char inChar);

    void setCustomDataCalback(void (* calback)(char indata));


    int buttonCount() { return 8; }

private:
    bool validButton(int button);

    enum ReadStatus {
        FindHeader1,
        FindHeader2,
        Command,
        ReadAxis1,
        ReadAxis2,
        ReadAxis3,
        ReadAxis4,
        ReadButtons,
        ReadCustomData
    };

    enum Commands {
        CmdJoystick,
        CmdRequestSetup,
        CmdCustomData,
    };

    struct Button {
        Button(): pressed(false), pressedOld(false) {}
        bool pressed;
        bool pressedOld;
        String text;
    };

    int m_readState;
    char m_axis1;
    char m_axis2;
    char m_axis3;
    char m_axis4;
    Button m_buttons[8];
    bool m_newInputData;

    void (* m_calback)(char indata);
};

#endif





